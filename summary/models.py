# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class Summary(models.Model):
    name = models.CharField()
    paydate = models.DateField(blank=True, null=True)
    gross = models.DecimalField(max_digits=10, decimal_places=2, blank=True, null=True)
    fedtax = models.DecimalField(max_digits=10, decimal_places=2, blank=True, null=True)
    sstax = models.DecimalField(max_digits=10, decimal_places=2, blank=True, null=True)
    medtax = models.DecimalField(max_digits=10, decimal_places=2, blank=True, null=True)
    sit = models.DecimalField(max_digits=10, decimal_places=2, blank=True, null=True)
    netpay = models.DecimalField(max_digits=10, decimal_places=2, blank=True, null=True)
    fica = models.DecimalField(max_digits=10, decimal_places=2, blank=True, null=True)
    futa = models.DecimalField(max_digits=10, decimal_places=2, blank=True, null=True)
    sui = models.DecimalField(max_digits=10, decimal_places=2, blank=True, null=True)
    employee_id = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'summary'
