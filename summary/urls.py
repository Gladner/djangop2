from django.urls import path
from .views import SummaryList


urlpatterns = [
    path('summary/', SummaryList.as_view(), name='summary'),
]
