from .models import Summary
from django.views.generic.list import ListView


class SummaryList(ListView):
    model = Summary
